/*******************************************************************************
 Copyright (c) 2013 Koninklijke Philips N.V.
 All Rights Reserved.
 ********************************************************************************/

#import <UIKit/UIKit.h>

@interface PHControlLightsViewController : UIViewController

- (void) randomizeWithAppleWatch;
- (void) changeBrightness:(int)brightValue;
- (void) changeSaturation:(int)saturationValue;
- (void) changeHue:(int)hueValue;
- (void) changeOnStatus: (NSNumber *)status;
- (void) playSong: (int)indexOfSong;
- (void) blinkLight;
- (void) startRangingBeacon;
- (void) sendLocalNotificationWithText: (NSString *)notificationText andDelay:(int)delay;

@property (weak, nonatomic) IBOutlet UILabel *beaconResult;

@end
