//
//  InterfaceController.m
//  AppleWatchHueControl WatchKit Extension
//
//  Created by RM on 09/06/2015.
//  Copyright (c) 2015 Philips. All rights reserved.
//

#import "InterfaceController.h"
#import <AVFoundation/AVFoundation.h>




@interface InterfaceController()

@property (strong, nonatomic) AVSpeechSynthesizer *synthesizer;

@end


@implementation InterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    // Configure interface objects here.
}
- (IBAction)randomizeLights:(id)sender {
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:@"randomizeColor" forKey:@"commandType"];

    [InterfaceController openParentApplication:dict reply:^(NSDictionary *replyInfo, NSError *error) {  }];
}

- (IBAction)vocalControl {
    [self presentTextInputControllerWithSuggestions:nil allowedInputMode:WKTextInputModePlain completion:^(NSArray *results) {
        // Do whatever you like with the array of results.
        NSString *vocal = [results objectAtIndex:0];
        NSString *speechFeedback;
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        // COMMAND TO RANDOMIZE THE COLOR
        if ([vocal containsString:@"Change"]) {
            [dict setValue:@"randomizeColor" forKey:@"commandType"];
            speechFeedback = @"Choosing a random color";
        }
        
        // COMMANDS TO CHANGE TO SPECIFIC COLOR
        else if ([vocal containsString:@"Red"] || [vocal containsString:@"Read"] ) {
            [dict setValue:@"setColor" forKey:@"commandType"];
            [dict setValue:@"red" forKey:@"desiredColor"];
            speechFeedback = @"Setting lights to red";
        }
        
        else if ([vocal containsString:@"Blue"]) {
            [dict setValue:@"setColor" forKey:@"commandType"];
            [dict setValue:@"blue" forKey:@"desiredColor"];
            speechFeedback = @"Setting lights to blue";
        }
        
        else if ([vocal containsString:@"Yellow"]) {
            [dict setValue:@"setColor" forKey:@"commandType"];
            [dict setValue:@"yellow" forKey:@"desiredColor"];
            speechFeedback = @"Setting lights to yellow";
        }
        
        else if ([vocal containsString:@"Porn"] || [vocal containsString:@"Born"] || [vocal containsString:@"Purple"]) {
            [dict setValue:@"setColor" forKey:@"commandType"];
            [dict setValue:@"purple" forKey:@"desiredColor"];
            speechFeedback = @"Sexy mode on.";
        }
        
        else if ([vocal containsString:@"Green"]) {
            [dict setValue:@"setColor" forKey:@"commandType"];
            [dict setValue:@"green" forKey:@"desiredColor"];
            speechFeedback = @"Setting lights to green";
        }
        
        // COMMANDS TO CHANGE ON OR OFF STATUS
        else if ([vocal containsString:@"On"]) {
            [dict setValue:@"setOn" forKey:@"commandType"];
            speechFeedback = @"Turning on lights";
        }
        
        else if ([vocal containsString:@"Off"] || [vocal containsString:@"Of"]) {
            [dict setValue:@"setOff" forKey:@"commandType"];
            speechFeedback = @"Turning off lights";
        }
        
        // COMMANDS TO BLINK
        else if ([vocal containsString:@"Blink"] || [vocal containsString:@"Alert"]) {
            [dict setValue:@"blink" forKey:@"commandType"];
            
        }
        
        if ([speechFeedback isEqualToString:@""] == FALSE) {
            [InterfaceController openParentApplication:dict reply:^(NSDictionary *replyInfo, NSError *error) {  }];
            AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:speechFeedback];
            utterance.rate = AVSpeechUtteranceDefaultSpeechRate;
            utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-AU"];
            [self.synthesizer speakUtterance:utterance];
        }
    }];

}


- (IBAction)changeBrightness:(float)value {
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:[NSNumber numberWithFloat:value] forKey:@"brightnessValue"];
    [dict setValue:@"changeBrightness" forKey:@"commandType"];
    
    [InterfaceController openParentApplication:dict reply:^(NSDictionary *replyInfo, NSError *error) {  }];    
}


- (IBAction)changeSaturation:(float)value {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:[NSNumber numberWithFloat:value] forKey:@"saturationValue"];
    [dict setValue:@"changeSaturation" forKey:@"commandType"];
    [InterfaceController openParentApplication:dict reply:^(NSDictionary *replyInfo, NSError *error) {  }];
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    self.synthesizer = [[AVSpeechSynthesizer alloc] init];
    
    
    }

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



