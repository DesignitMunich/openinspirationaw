/*******************************************************************************
 Copyright (c) 2013 Koninklijke Philips N.V.
 All Rights Reserved.
 ********************************************************************************/

#import "PHControlLightsViewController.h"
#import "PHAppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import "APLDefaults.h"
#import <HueSDK_iOS/HueSDK.h>
#import <MediaPlayer/MediaPlayer.h>



#define MAX_HUE 65535

@interface PHControlLightsViewController() <CLLocationManagerDelegate>

@property (nonatomic,weak) IBOutlet UILabel *bridgeMacLabel;
@property (nonatomic,weak) IBOutlet UILabel *bridgeIpLabel;
@property (nonatomic,weak) IBOutlet UILabel *bridgeLastHeartbeatLabel;
@property (nonatomic,weak) IBOutlet UIButton *randomLightsButton;

@property NSMutableDictionary *beacons;
@property CLLocationManager *locationManager;
@property NSMutableDictionary *rangedRegions;

@property NSInteger *prototypeGroupNumber;


@end


@implementation PHControlLightsViewController

int selectedHue;
int selectedBrightness;
int selectedSaturation;
BOOL notificationForInside = FALSE;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    PHAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.controlLightsViewController = self;
    
    self.prototypeGroupNumber = 0;
    
    PHNotificationManager *notificationManager = [PHNotificationManager defaultManager];
    // Register for the local heartbeat notifications
    [notificationManager registerObject:self withSelector:@selector(localConnection) forNotification:LOCAL_CONNECTION_NOTIFICATION];
    [notificationManager registerObject:self withSelector:@selector(noLocalConnection) forNotification:NO_LOCAL_CONNECTION_NOTIFICATION];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Find bridge" style:UIBarButtonItemStylePlain target:self action:@selector(findNewBridgeButtonAction)];
    
    self.navigationItem.title = @"Designit RP";
    
    [self noLocalConnection];
    
    
    // *****************************************************
    // DEFAULT VALUE FOR LAMP
    // *****************************************************
    selectedHue = 11000;
    selectedBrightness = 254;
    selectedSaturation = 254;
    [self changeLightStatus];
    
    // *****************************************************
    // LOCATION MANAGER AND BEACON SETUP
    // *****************************************************
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    self.rangedRegions = [[NSMutableDictionary alloc] init];
    self.beacons = [[NSMutableDictionary alloc] init];
    
    for (NSUUID *uuid in [APLDefaults sharedDefaults].supportedProximityUUIDs)
    {
        CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:[uuid UUIDString]];
        self.rangedRegions[region] = [NSArray array];
    }
}

- (UIRectEdge)edgesForExtendedLayout {
    return UIRectEdgeLeft | UIRectEdgeBottom | UIRectEdgeRight;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)localConnection{
    
    [self loadConnectedBridgeValues];
    
}

- (void)noLocalConnection{
    self.bridgeLastHeartbeatLabel.text = @"Not connected";
    [self.bridgeLastHeartbeatLabel setEnabled:NO];
    self.bridgeIpLabel.text = @"Not connected";
    [self.bridgeIpLabel setEnabled:NO];
    self.bridgeMacLabel.text = @"Not connected";
    [self.bridgeMacLabel setEnabled:NO];
    
    [self.randomLightsButton setEnabled:NO];
}

- (void)loadConnectedBridgeValues{
    PHBridgeResourcesCache *cache = [PHBridgeResourcesReader readBridgeResourcesCache];
    
    // Check if we have connected to a bridge before
    if (cache != nil && cache.bridgeConfiguration != nil && cache.bridgeConfiguration.ipaddress != nil){
        
        // Set the ip address of the bridge
        self.bridgeIpLabel.text = cache.bridgeConfiguration.ipaddress;
        
        // Set the mac adress of the bridge
        self.bridgeMacLabel.text = cache.bridgeConfiguration.mac;
        
        // Check if we are connected to the bridge right now
        if (UIAppDelegate.phHueSDK.localConnected) {
            
            // Show current time as last successful heartbeat time when we are connected to a bridge
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateStyle:NSDateFormatterNoStyle];
            [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
            
            self.bridgeLastHeartbeatLabel.text = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
            
            [self.randomLightsButton setEnabled:YES];
        } else {
            self.bridgeLastHeartbeatLabel.text = @"Waiting...";
            [self.randomLightsButton setEnabled:NO];
        }
    }
}

- (IBAction)selectOtherBridge:(id)sender{
    [UIAppDelegate searchForBridgeLocal];
}


- (IBAction)randomizeColoursOfConnectLights:(id)sender{
    // [self.randomLightsButton setEnabled:NO];
    
    
    selectedHue = arc4random() % MAX_HUE;
    [self changeLightStatus];
    
    /*
    PHBridgeResourcesCache *cache = [PHBridgeResourcesReader readBridgeResourcesCache];
    PHBridgeSendAPI *bridgeSendAPI = [[PHBridgeSendAPI alloc] init];
    
    
    
    for (PHLight *light in cache.lights.allValues) {
        
        PHLightState *lightState = [[PHLightState alloc] init];
        
        [lightState setHue:[NSNumber numberWithInt:arc4random() % MAX_HUE]];
        [lightState setBrightness:[NSNumber numberWithInt:254]];
        [lightState setSaturation:[NSNumber numberWithInt:254]];
        
        // Send lightstate to light
        [bridgeSendAPI updateLightStateForId:light.identifier withLightState:lightState completionHandler:^(NSArray *errors) {
            if (errors != nil) {
                NSString *message = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Errors", @""), errors != nil ? errors : NSLocalizedString(@"none", @"")];
                
                NSLog(@"Response: %@",message);
            }
            
            [self.randomLightsButton setEnabled:YES];
        }];
    }
     */
}

- (void)findNewBridgeButtonAction{
    [UIAppDelegate searchForBridgeLocal];
}

- (IBAction)selectPrototypingGroup:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    self.prototypeGroupNumber = segmentedControl.selectedSegmentIndex;
}


- (void) changeLightStatus {
    PHBridgeResourcesCache *cache = [PHBridgeResourcesReader readBridgeResourcesCache];
    PHBridgeSendAPI *bridgeSendAPI = [[PHBridgeSendAPI alloc] init];
    
    
    // PHLight *light = [cache.lights objectForKey:@"1"];
    
    /*
     
     Desk-Lamp = 6
     Floor-Lamp = 9
     Other Lamps from Window to Aisle = 4,2,0,7,5,3,1,8
     */
    
    NSArray *groupLampIdxs;
    
    if(self.prototypeGroupNumber == 1) {
        groupLampIdxs = [NSArray arrayWithObjects:
                         [NSNumber numberWithInteger:4],
                         [NSNumber numberWithInteger:2],
                         [NSNumber numberWithInteger:0],
                         [NSNumber numberWithInteger:7],
                         nil];
    } else {
        groupLampIdxs = [NSArray arrayWithObjects:
                         [NSNumber numberWithInteger:5],
                         [NSNumber numberWithInteger:3],
                         [NSNumber numberWithInteger:1],
                         [NSNumber numberWithInteger:8],
                         nil];
    }
    
    
    
    NSNumber *lightIndex = [NSNumber numberWithInteger:-1];
    for (PHLight *light in cache.lights.allValues) {
        int value = [lightIndex intValue];
        lightIndex = [NSNumber numberWithInt:value + 1];
        
        NSInteger anIndex=[groupLampIdxs indexOfObject:lightIndex];
        if(NSNotFound != anIndex) {
            PHLightState *lightState = [[PHLightState alloc] init];
            
            [lightState setHue:[NSNumber numberWithInt:selectedHue]];
            
            [lightState setBrightness:[NSNumber numberWithInt:selectedBrightness]];
            [lightState setSaturation:[NSNumber numberWithInt:selectedSaturation]];
            [lightState setTransitionTime:[NSNumber numberWithInt:10]];
            
            
            // Send lightstate to light
            [bridgeSendAPI updateLightStateForId:light.identifier withLightState:lightState completionHandler:^(NSArray *errors) {
                if (errors != nil) {
                    NSString *message = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Errors", @""), errors != nil ? errors : NSLocalizedString(@"none", @"")];
                    
                    NSLog(@"Response: %@",message);
                }
                
            }];
        }
    }
    
}



- (void) randomizeWithAppleWatch {
    selectedHue = arc4random() % MAX_HUE;
    // selectedBrightness = 254;
    // selectedSaturation = 254;
    
    [self changeLightStatus];
}


- (void)changeBrightness:(int)brightValue {
    
    /*
    selectedBrightness = brightValue;
    [self changeLightStatus];
     */
}


- (void)changeHue:(int)hueValue {
    
    /*
    selectedHue = hueValue;
    [self changeLightStatus];
     */
}


- (void) changeSaturation:(int)saturationValue {
    
    /*
    selectedSaturation = saturationValue;
    [self changeLightStatus];
    */
}


- (void) blinkLight {
    
    /*
    [self changeOnStatus:@NO];
    [self performSelector:@selector(changeOnStatus:) withObject:@YES afterDelay:1];
    [self performSelector:@selector(changeOnStatus:) withObject:@NO afterDelay:2];
    [self performSelector:@selector(changeOnStatus:) withObject:@YES afterDelay:3];
    [self performSelector:@selector(changeOnStatus:) withObject:@NO afterDelay:4];
    [self performSelector:@selector(changeOnStatus:) withObject:@YES afterDelay:5];
    [self performSelector:@selector(changeOnStatus:) withObject:@NO afterDelay:6];
    [self performSelector:@selector(changeOnStatus:) withObject:@YES afterDelay:7];
    [self performSelector:@selector(changeOnStatus:) withObject:@NO afterDelay:8];
    [self performSelector:@selector(changeOnStatus:) withObject:@YES afterDelay:9];
     
     */
}


- (void) playSong:(int)indexOfSong {
    
    /*
    MPMusicPlayerController *musicPlayer = [MPMusicPlayerController applicationMusicPlayer];
    MPMediaPropertyPredicate *artist = [MPMediaPropertyPredicate predicateWithValue:[songsTitle objectAtIndex:indexOfSong] forProperty:MPMediaItemPropertyTitle];
    
    MPMediaQuery *myArtistQuery = [[MPMediaQuery alloc] init];
    [myArtistQuery addFilterPredicate:artist];
    
    [musicPlayer setQueueWithQuery:myArtistQuery];
    [musicPlayer play];
    */
}


- (void) changeOnStatus: (NSNumber *)status {
    PHBridgeResourcesCache *cache = [PHBridgeResourcesReader readBridgeResourcesCache];
    PHBridgeSendAPI *bridgeSendAPI = [[PHBridgeSendAPI alloc] init];
    
    for (PHLight *light in cache.lights.allValues) {
        
        PHLightState *lightState = [[PHLightState alloc] init];
        
        lightState.on = status;
        [lightState setBrightness:[NSNumber numberWithInt:selectedBrightness]];
        
        // Send lightstate to light
        [bridgeSendAPI updateLightStateForId:light.identifier withLightState:lightState completionHandler:^(NSArray *errors) {
            if (errors != nil) {
                NSString *message = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Errors", @""), errors != nil ? errors : NSLocalizedString(@"none", @"")];
                
                NSLog(@"Response: %@",message);
            }
            
            // [self.randomLightsButton setEnabled:YES];
        }];
    }
}


- (void) startRangingBeacon {
    self.locationManager.pausesLocationUpdatesAutomatically = NO;
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager requestAlwaysAuthorization];
    
    for (CLBeaconRegion *region in self.rangedRegions)
    {
        [self.locationManager startMonitoringForRegion:region];
        [self.locationManager startRangingBeaconsInRegion:region];
    }
    [self.locationManager startUpdatingLocation];
}



#pragma mark - Location manager delegate

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    
    NSLog(@"RANGING BEACONS");
    if ([beacons count] > 0) {
        
        // [self.beaconResult setText:@"FOUND A BEACON"];
        
        NSMutableArray *immediateBeacons = [NSMutableArray array];
        
        for (int i=0; i<[beacons count]; i++) {
            
            CLBeacon *beaconToCheck = [beacons objectAtIndex:i];
            
            if (beaconToCheck.proximity == CLProximityNear || beaconToCheck.proximity == CLProximityImmediate) {
                [immediateBeacons addObject:[beacons objectAtIndex:i]];
            }
        }
        
        
        CLBeacon *nearestExhibit = [immediateBeacons firstObject];
        
        
        /*
        if ([nearestExhibit.proximityUUID.UUIDString isEqualToString:@"A77A1B68-49A7-4DBF-914C-760D07FBB87B"])  {
        }
        else {
        }
        */
        
        // Present the exhibit-specific UI only when
        // the user is relatively close to the exhibit.
        if (nearestExhibit.proximity == CLProximityNear || nearestExhibit.proximity == CLProximityImmediate) {
            if (notificationForInside == FALSE) {
                [self sendLocalNotificationWithText:@"Some entered my home" andDelay:5];
                notificationForInside = TRUE;
                [self blinkLight];
            }
            
        }
        else {
            notificationForInside = FALSE;
            NSLog(@"BEACON FAR");
        }
        
    }
    
}

- (void) sendLocalNotificationWithText: (NSString *)notificationText andDelay:(int)delay {
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:delay];
    localNotification.alertBody = notificationText;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
}



@end
