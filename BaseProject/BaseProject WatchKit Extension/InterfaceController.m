//
//  InterfaceController.m
//  BaseProject WatchKit Extension
//
//  Created by RM on 16/06/2015.
//  Copyright (c) 2015 Philips. All rights reserved.
//

#import "InterfaceController.h"


@interface InterfaceController()

@end


@implementation InterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}


- (void) exampleTrigger:(float)value {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:[NSNumber numberWithFloat:value] forKey:@"saturationValue"];
    [dict setValue:@"changeSaturation" forKey:@"commandType"];
    [InterfaceController openParentApplication:dict reply:^(NSDictionary *replyInfo, NSError *error) {  }];
}

- (void) exampleVocal {
    [self presentTextInputControllerWithSuggestions:nil allowedInputMode:WKTextInputModePlain completion:^(NSArray *results) {
        // Do whatever you like with the array of results.
        NSString *vocal = [results objectAtIndex:0];
        NSString *speechFeedback;
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        // COMMAND TO RANDOMIZE THE COLOR
        if ([vocal containsString:@"Change"]) {
            [dict setValue:@"randomizeColor" forKey:@"commandType"];
            [InterfaceController openParentApplication:dict reply:^(NSDictionary *replyInfo, NSError *error) {  }];
        }
    }];
}
- (IBAction)checkDistancePressed {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:@"startRanging" forKey:@"commandType"];
    [InterfaceController openParentApplication:dict reply:^(NSDictionary *replyInfo, NSError *error) {  }];
    
}

@end



