//
//  InterfaceController.h
//  BaseProject WatchKit Extension
//
//  Created by RM on 16/06/2015.
//  Copyright (c) 2015 Philips. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface InterfaceController : WKInterfaceController

@end
